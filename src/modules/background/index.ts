`
contextMenus:
  - 
    id: test
    title: 测试
    contexts:
      - selection
    onclick: handleNameMenuClick
    callback: handleNameMenuCreateCallback
`;

console.log('Hello, world!');

function handleNameMenuClick() {
  console.log(`Menu Clicked!`);
}

function handleNameMenuCreateCallback() {
  console.log(`Menu Created!`);
}
