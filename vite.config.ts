import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { nusuthPlugin } from 'vite-plugin-crx-nusuth'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), nusuthPlugin()],
})
